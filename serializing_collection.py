from datetime import datetime
from decimal import Decimal

from pydantic import BaseModel, TypeAdapter


class Dish(BaseModel):
    name: str
    description: str
    price: Decimal


class Order(BaseModel):
    dishes: list[Dish]
    opened: datetime


order = Order(
    dishes=[
        Dish(name="lasagna", description="tasty", price=Decimal("12.5")),
        Dish(name="fish", description="fresh", price=Decimal("15.5")),
    ],
    opened=datetime.now(),
)


ta = TypeAdapter(list[Dish])
json_bytes = ta.dump_json(order.dishes, indent=4)
print(json_bytes.decode())
